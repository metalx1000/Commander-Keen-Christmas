extends Node2D

export var points = 10
export var health = 0

func _ready():
	pass # Replace with function body.



func pickup(body):
	if !body.is_in_group("players"):
		return
		
	body.health += health
	body.score += points
	
	queue_free()
