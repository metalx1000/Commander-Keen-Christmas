extends Node2D

var cheatstr = ""


func _input(ev):
	if ev.is_pressed():
		var key = ev.as_text()
		#if key other then letter clear string
		if key.length() > 1:
			cheatstr = ""
		else:
			cheatstr += key.to_lower()
			check_cheat()

func check_cheat():
	if "iddqd" in cheatstr:
		cheatstr = ""
		print("god mode")
	elif "idkfa" in cheatstr:
		cheatstr = ""
		print("All Keys, Weapons, and Ammo")
