extends KinematicBody2D

export (int) var speed = 3200
export (int) var jump_speed = -400
export (int) var gravity = 1000

onready var ray = $RayCast2D
onready var ray2 = $RayCast2D2
onready var hud = $Label

var score = 0
var health = 100

var velocity = Vector2.ZERO

onready var sprite=$sprite

func _ready():
	#set_color(.8)
	pass
	
func _physics_process(delta):
	update_hud()
	get_input(delta)
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector2.UP)
	jump_attack()

func set_score(value = 1):
	score+=value

func update_hud():
	hud.text = "Score: " + str(score);

func set_color(v):
	# Duplicate the shader so that changing its param doesn't change it on any other sprites that also use the shader.
	# Generally done once in _ready()
	sprite.set_material(sprite.get_material().duplicate(true))

	# Offset sprite hue by a random value within specified limits.
	#var rand_hue = float(randi() % 3)/2.0/3.2
	sprite.material.set_shader_param("Shift_Hue", v)

func jump_attack():
	if ray.is_colliding():
		attack(ray.get_collider()) 

	elif ray2.is_colliding():
		attack(ray2.get_collider()) 

func attack(obj):
	
	if obj.has_method("take_damage"):
		obj.take_damage(1)
		velocity.y = jump_speed / 2
		score+=10
		
func get_input(delta):
	velocity.x = 0
	
	if Input.is_action_pressed("ui_right"):
		sprite.flip_h = false
		sprite.play("walk")
		velocity.x += speed * delta * 2
	if Input.is_action_pressed("ui_left"):
		sprite.flip_h = true
		sprite.play("walk") 
		velocity.x -= speed * delta * 2
		
	if velocity.x < 30 && velocity.x > -30:
		sprite.play("default")
		
	if Input.is_action_just_pressed("ui_up"):
		if is_on_floor():
			$jump_snd.play()
			velocity.y = jump_speed
