extends KinematicBody2D


export (bool) var active = false
export (int) var speed = 1000
export (int) var gravity = 1000
export (int) var direction = 1
export (bool) var can_fall = false
export (bool) var sprite_flip = false
export (int) var updown = 0
export (int) var health = 1

var updown_wait = 0

var velocity = Vector2.ZERO

var explosion = load("res://objects/explosion/explosion.tscn")
#var explosion = load("res://objects/explosion/explosion_2.tscn")
var explode = explosion.instance()

onready var sprite = $sprite
onready var offscreen_timer =$offscreen_timer
onready var player = $AnimationPlayer
onready var attack_timer = $attack_timer
var mover = Vector2.ZERO

func _ready():
	randomize()
	attack_timer.wait_time = rand_range(3,5)
	
func  _physics_process(delta):
	velocity.x = 0
	if active:
		velocity.y += gravity * delta
		velocity = move_and_slide(velocity, Vector2.UP)
		

func facing():
	if direction == 1:
		if sprite_flip:
			sprite.flip_h = true
		else:
			sprite.flip_h = false
	else:
		if sprite_flip:
			sprite.flip_h = false
		else:
			sprite.flip_h = true


func _on_VisibilityNotifier2D_viewport_entered(viewport):
	offscreen_timer.stop()
	active = true


func attack():
	player.play("default")
	attack_timer.wait_time = rand_range(3,5)
	
func _on_VisibilityNotifier2D_viewport_exited(viewport):
	offscreen_timer.start()
	
	
func _on_offscreen_timer_timeout():
	#if gunner us off screen for 5 seconds then set to inactive
	active = false

func take_damage(amount):
	health -= 1
	if health < 1:
		death()

func death():
	get_tree().get_current_scene().add_child(explode)
	explode.position = position
	explode.scale.x = .4
	explode.scale.y = .4
	queue_free()


func _on_Area2D_area_shape_entered(area_id, area, area_shape, self_shape):
	if area.is_in_group("explosion"):
		death()


